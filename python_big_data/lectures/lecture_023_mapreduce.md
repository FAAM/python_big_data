# MapReduce

## Introducción

**MapReduce** es un modelo o patrón de programación dentro del marco de [Hadoop](http://hadoop.apache.org/) que se utiliza para acceder a macrodatos almacenados en el sistema de archivos Hadoop ([HDFS](https://hadoop.apache.org/docs/r1.2.1/hdfs_design.html)). Es un componente central, integral para el funcionamiento del marco de Hadoop.

MapReduce facilita el procesamiento simultáneo al dividir petabytes de datos en fragmentos más pequeños y procesarlos en paralelo en los servidores básicos de Hadoop. Al final, agrega todos los datos de varios servidores para devolver una salida consolidada a la aplicación.

Por ejemplo, un clúster de Hadoop con 20.000 servidores básicos de bajo costo y un bloque de datos de 256 MB en cada uno, puede procesar alrededor de 5 TB de datos al mismo tiempo. Esto reduce el tiempo de procesamiento en comparación con el procesamiento secuencial de un conjunto de datos tan grande.

Con MapReduce, en lugar de enviar datos a donde reside la aplicación o la lógica, la lógica se ejecuta en el servidor donde ya residen los datos, para acelerar el procesamiento. El acceso y almacenamiento de datos se basa en disco; la entrada generalmente se almacena como archivos que contienen datos estructurados, semiestructurados o no estructurados, y la salida también se almacena en archivos.

MapReduce fue una vez el único método a través del cual se podían recuperar los datos almacenados en HDFS, pero ese ya no es el caso. Hoy en día, existen otros sistemas basados en consultas, como [Hive](https://hive.apache.org/), que se utilizan para recuperar datos del HDFS mediante declaraciones similares a SQL. Sin embargo, estos generalmente se ejecutan junto con trabajos escritos con el modelo MapReduce.

## Paper

La primera vez que se habla del término **Mapreduce** es en el paper denominado [MapReduce: Simplified Data Processing on Large Clusters](https://gitlab.com/FAAM/python_big_data/-/blob/master/python_big_data/lectures/data/mapreduce-osdi04.pdf) por Jeffrey Dean & Sanjay Ghemawat. Acá se deja el abstract del trabajo:

```{note}
MapReduce is a programming model and an associated implementation for processing and generating large datasets that is amenable to a broad variety of real-world tasks. Users specify the computation in terms of a map and a reduce function, and the underlying runtime system automatically parallelizes the computation across large-scale clusters of machines, handles machine failures, and schedules inter-machine communication to make efficient use of the network and disks. Programmers find the system easy to use: more than ten thousand distinct MapReduce programs have been implemented internally at Google over the past four years, and an average of one hundred thousand MapReduce jobs are executed on Google's clusters every day, processing a total of more than twenty petabytes of data per day.
```

### Review

A continuación se hace una breve reseña del paper.

<img src="images/mapreduce_01.png" alt="" align="center"/>


### Resumen
 Se introduce un marco de programación, MapReduce, para procesar fácilmente cálculos a gran escala en grandes grupos de PC básicos. En un modelo MapReduce, los usuarios utilizan una función de mapa autodefinida en los trabajadores de mapeo para procesar divisiones de datos de entrada, generando un conjunto de pares clave / valor intermedios y usan una función de reducción para reducir trabajadores para ordenar y fusionar los valores intermedios con la misma clave . El documento también proporciona algunos ejemplos de modelos de programación e implementaciones típicas de MapReduce que procesan terabytes de datos en miles de máquinas. Algunas mejoras del modelo, la evaluación del desempeño de la implementación, el uso de MapReduce en el sistema de indexación dentro de Google y el trabajo relacionado y futuro también se discuten en el documento.

### Problema

Hay grandes cantidades de datos para procesar y generar en muchas tareas del mundo real. Aunque la mayoría de los cálculos son sencillos, para mejorar la eficiencia, deben distribuirse entre miles de máquinas debido a la enorme cantidad de datos. Los métodos que distribuyen los datos, paralelizan los cálculos y manejan fallas involucran grandes cantidades de código complicado. El problema es cómo implementar esos trabajos de una manera simple y poderosa y hacerlos tolerantes a fallas incluso en numerosas máquinas.

### Solución 

Los autores diseñaron un marco que dividía las tareas de paralelización y distribución en las fases de Mapeo y Reducción en general. El maestro asigna tareas de mapeo y reducción de tareas a los trabajadores inactivos. En la fase de mapeo, los datos de entrada se dividen en varias divisiones, y esas divisiones pueden ser procesadas por diferentes máquinas (mapeadores) en paralelo, generando pares de clave / valor intermedios que se almacenan en búfer en el disco local. En la fase Reducción, los trabajadores reducen leer pares intermedios de forma remota y ordenarlos por claves para que aquellos con la misma clave se agreguen, y luego pasar la clave y la lista de valores correspondiente a la función Reducir definida por el usuario, que devuelve un salida adjunta a un archivo de salida para esa partición de reducción. Todos los detalles confusos del particionamiento, la programación de la ejecución, las comunicaciones entre máquinas, la tolerancia a fallas y el equilibrio de carga están ocultos en una biblioteca y acomodados por el sistema de tiempo de ejecución. Esto hace que la interfaz de MapReduce sea simple, manejable y poderosa incluso para programadores sin experiencia en sistemas paralelos y distribuidos.


<img src="images/mapreduce_02.png" alt="" align="center"/>

### Novedad
MapReduce es una nueva solución de marco de cálculo distribuido. Está inspirado en las funciones primitivas map y reduce en muchos lenguajes de programación. MapReduce es simple y más tolerante a fallas, en comparación con muchos otros sistemas de procesamiento paralelo contemporáneos, estos también son modelos de programación restringidos pero solo se han implementado en escalas más pequeñas y dejan las fallas de la máquina a los programadores para que las manejen. La optimización de la localidad, el mecanismo de tareas de respaldo y la función de clasificación de MapReduce tienen similitudes en espíritu con otras técnicas, pero se extienden desde diferentes enfoques.

### Evaluación

Los autores evaluaron el rendimiento de MapReduce con dos cálculos de ejemplo que se ejecutan en un gran grupo de PC. Uno busca un patrón específico a través de aproximadamente un terabyte de datos, el otro clasifica aproximadamente un terabyte de datos. El primero muestra el progreso del programa grep en el aspecto de la tasa de transferencia de datos. Este último muestra cómo la desactivación de las tareas de respaldo puede ralentizar la ejecución en comparación con la ejecución normal y demostró las fortalezas del manejo de fallas de la máquina. Las diferencias entre MapReduce y otros métodos existentes se discuten principalmente en términos de principios. En general, MapReduce es más simple (amigable para el programador), más tolerante a fallas y extensible que otros sistemas de procesamiento paralelo.

### Opinión
En la sección de Performance, las mediciones de dos implementaciones (búsqueda y clasificación) solo se enfocan en las fortalezas del modelo MapReduce y las debilidades apenas se discuten. Además, sería mejor si hubiera algunas comparaciones con métodos de paralelización similares hacia las mismas tareas. Una de las partes más convincentes para es la sección Experiencia, que indica los avances y mejoras de MapReduce y su amplia gama de aplicaciones dentro de Google, especialmente el sistema de indexación a gran escala.


#### Key Points

* MapReduce es un modelo de programación para procesar y generar grandes conjuntos de datos.
* Input -> splitting - -(k/v pairs)- -> **Mapping** - -(k/v pairs)- -> shuffling -> **Reducing** -> Output
* Tres estados para las tareas: inactivo, en curso, completo; mantenido por el maestro junto con la identidad de los trabajadores.
* Todo lo que esté en progreso sobre los trabajadores fallidos y las tareas de mapeo completadas se restablecerá y se reprogramará; aunque no es necesario completar las tareas de reducción, ya que la salida ya está almacenada en un sistema de archivos global.

#### Pros
* Interfaz simple y potente, oculta detalles de paralelización y distribución.
* Alta tolerancia a fallas, el maestro maneja las fallas de los trabajadores reasignando trabajos
* Altamente escalable, se puede implementar en un clúster grande

#### Contras

* Limitado para algoritmos iterativos y consultas de datos interactivas debido a la alta replicación y I/O de disco
* Puede ser de baja eficiencia debido a rezagados (máquinas lentas)
* No es compatible con lenguaje de alto nivel, es decir, SQL
