<!-- #region -->
# Tipos de Formatos

## Introducción

Un formato se refiere a forma, básicamente es la definición de elementos, la disposición en que son organizados y almacenados. Estos, son diseñados con características en función de lograr objetivos específicos, por ejemplo un fichero `.jpeg` no persigue el mismo fin que un `.gif`, un documento de texto de Microsoft Office `.doc` de igual forma difiere de una hoja de cálculos `.xls`. Incluso entre versiones de estos formatos se hacen mejoras para optimizar sus características, por ejemplo Microsoft Office de 2003 en adelante.

En áreas analíticas se dan diferentes escenarios de acceso y consumo de los datos de la organización. Escenarios evidentemente marcados por las condiciones tecnológicas y madurez en gestión de la información.


## Basado en filas vs columnas

Esta problemática no es exclusiva de los formatos de fichero, también los es de los sistemas gestores de bases de datos (**SGBD**). Precisamente este es uno de los criterios que dio origen a los hoy conocidos sistemas de bases de datos NoSQL y sus diferentes tipos.

La gran mayoría de los SGDB relacionales son basados en filas, en cambio entre los gestores NoSQL existen 4 grandes tipos que podemos destacar: documentales, columnares, basados en grafo y clave-valor. Los sistemas relaciones están pensados para entornos OLTP (Online Transaction Processing), en cambio los sistemas NoSQL, si bien son ampliamente utilizados en OLTP  tienen su mayor potencial en entornos OLAP (Online analytical processing ) o de procesamiento de grandes volúmenes de datos.

Esto no significa que los NoSQL son una bala de plata, al contrario todas las ventajas de rendimiento que ofrecen sobre los SGBD relacionales lo hacen en parte sacrificando integridad y si bien existen técnicas de modelamiento entidad-relación para sistemas NoSQL, lo cierto es que no resultan tan amigables como en un sistema relacional. Para combinar las mejores características de ambos mundos se han diseñado las NewSQL, otro tipo de SGDB.

Para poder comparar el funcionamiento entre los SGBD basados en filas y los columnares es importante entender cómo ocurre internamente el almacenamiento de los datos. En la siguiente imagen extraída de la Guía de Administración de SAP HANA se evidencia claramente su funcionamiento.

<img src="images/data_01.png" alt="" align="center"/>

Entre las principales ventajas que ofrece este tipo de almacenamiento se encuentran la velocidad de recuperación para columnas específicas y un alto factor de compresión.

Estas son características aplican de igual forma los formatos de ficheros basados en columnas.

## Formatos de fichero eficientes para analítica
El aumento prácticamente incontrolable del volumen de datos de las organizaciones y sobre todo las empresas de internet quienes fueron pioneras, llevó al desarrollo de nuevas tecnologías para almacenamiento y procesamiento distribuido como Hadoop y Apache Spark. Como parte de su evolución natural esto a la concepción de formatos de ficheros más eficientes tanto en su almacenamiento como durante el procesamiento.

Entre los formatos de ficheros más conocidos para analítica se encuentran **Avro**, **ORC** y **Parquet**. En el caso de Avro es un formato de ficheros basado en filas, mientras los ORC y Parquet son basados en columnas.

No pretendo entrar en detalles sobre las sutilezas de cada formato, pero básicamente existe una semejanza conceptual con los SGDB. En el caso de Avro es bastante simple, al ser basado en filas su factor de compresión es menor pero su velocidad de inserción es mayor y es recomendado para casos donde se van a recuperar toda la información contenida. En el caso de ORC y Parquet, ambos son columnares, su factor de compresión está determinado por el tipo de compresión que se seleccione (snappy, gzip, zlib) y están pensados para la recuperación de las columnas necesarias sin necesidad de recorrer toda la base.

La decisión entre usar ORC o Parquet no es tan trivial y tiene que ver incluso con su concepción, el primero fue desarrollado por Hortonworks + Facebook, mientras el otro por Cloudera + Twitter (aunque basado en tecnología de Google). En la actualidad la mayoría de los sistemas para análisis de grandes volúmenes de datos se podría decir que soportan por igual ambos formatos, pero en sus orígenes no fue así. Esto implicaba tener que mapear las tecnologías con que se iban a explorar los datos para poder seleccionar el formato adecuado. En el caso de la tecnología de Apache Spark, se podría decir que desde el inicio hicieron su mayor apuesta por Parquet.

<!-- #endregion -->

A continuación se muestran los resultados de la conversión de un fichero .csv de 55.9GB a estos formatos utilizando [Data Factory](https://azure.microsoft.com/en-us/services/data-factory/) en Microsoft Azure y almacenado en [Azure Data Lake Storage Gen2](https://azure.microsoft.com/en-us/services/storage/data-lake-storage/).

<img src="images/data_03.png" alt="" align="center"/>
<img src="images/data_02.png" alt="" align="center"/>


En el caso de la conversión a Parquet resultante en 2.8GB el algoritmo de compresión por defecto es snappy, al realizar el mismo ejercicio con gzip resultó en 185.0mb. En el caso de la conversión a Avro no se encontraron mejoras en el resultado.

## Ejemplos
A continuación veremos cómo trabajar con estos formatos de ficheros en nuestros entornos para Data Science, para esto nos auxiliaremos de [Apache Arrow](https://arrow.apache.org/). Sin entrar en una definición formal, se trata de una tecnología columnar de procesamiento en memoria, que a su vez permite procesamiento en tarjetas gráficas y actualmente cuenta con interfaces para 10 de lo más conocidos lenguajes de programación.

Apache Arrow además fue concebido como una tecnología de interoperabilidad, una capa de acceso a datos independiente de la plataforma como se muestra en la siguiente imagen tomada de su sitio web oficial.

<img src="images/data_04.png" alt="" align="left"/>


Actualmente Apache Arrow es utilizados por varias tecnologías de procesamiento distribuido para hacer el traspaso de los datos entre nodos eficientemente.

La forma más tradicional de utilizarlo, incluso en entornos standalone es desplegando Spark y ejecutándolo de forma local.

Personalmente encuentro sin mucho sentido ejecutar un sistema distribuido en un ordenador, por lo que esteramos mostrando cómo utilizarlo sin necesidad de desplegar Spark. Apache Arrow cuenta con sus propios tipos de datos y estructuras de modos que se pueden utilizar independiente de otros paquetes, sin embargo estaremos revisando cómo utilizarlo con los paquetes más comunes utilizados para Data Science.

En el caso de Python se puede instalar Apache Arrow desde cualquiera de los conocidos gestores de paquetes simplemente utilizando [pyarrow](https://arrow.apache.org/docs/python/index.html) en el caso de R existe de igual forma en el [CRAN](https://cran.r-project.org/web/packages/arrow/index.html) el paquete **arrow**, o con [reticulate](https://cran.r-project.org/web/packages/reticulate/index.html) para ejecutar código Python de R.

Con fines de demostración importaremos todos los paquetes necesarios desde el principio para poder utilizar numpy, pandas y las funciones de Apache Arrow.


```python
# librerias
import numpy as np
import pandas as pd
import pyarrow as pa
import pyarrow.parquet as pq
from pyarrow import csv
```


```python
# Importando fichero CSV y convirtiéndolo a DF de Pandas
file = 'data/demodata.csv'
table = csv.read_csv(file)
table
```




    pyarrow.Table
    ID: string
    FECHA: timestamp[s]
    VALOR: int64




```python
# Convirtiendo DF a Pandas.DataFrame
df = table.to_pandas()
df.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>ID</th>
      <th>FECHA</th>
      <th>VALOR</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>C1</td>
      <td>2018-01-01</td>
      <td>29</td>
    </tr>
    <tr>
      <th>1</th>
      <td>C2</td>
      <td>2018-01-02</td>
      <td>80</td>
    </tr>
    <tr>
      <th>2</th>
      <td>C3</td>
      <td>2018-01-03</td>
      <td>11</td>
    </tr>
    <tr>
      <th>3</th>
      <td>C4</td>
      <td>2018-01-04</td>
      <td>94</td>
    </tr>
    <tr>
      <th>4</th>
      <td>C5</td>
      <td>2018-01-05</td>
      <td>97</td>
    </tr>
  </tbody>
</table>
</div>




```python
# Convirtiendo DF de Pandas a Table de Arrow y exportando como fichero .parquet
table = pa.Table.from_pandas(df)
pq.write_table(table, 'demodata.parquet')
```


```python
#Importando fichero .parquet directo a DF de Pandas y filtando por columnas específicas
data = pq.read_pandas('demodata.parquet',columns=['ID', 'FECHA', 'VALOR']).to_pandas()
data.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>ID</th>
      <th>FECHA</th>
      <th>VALOR</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>C1</td>
      <td>2018-01-01</td>
      <td>29</td>
    </tr>
    <tr>
      <th>1</th>
      <td>C2</td>
      <td>2018-01-02</td>
      <td>80</td>
    </tr>
    <tr>
      <th>2</th>
      <td>C3</td>
      <td>2018-01-03</td>
      <td>11</td>
    </tr>
    <tr>
      <th>3</th>
      <td>C4</td>
      <td>2018-01-04</td>
      <td>94</td>
    </tr>
    <tr>
      <th>4</th>
      <td>C5</td>
      <td>2018-01-05</td>
      <td>97</td>
    </tr>
  </tbody>
</table>
</div>



Estos son los pasos más básicos, con Arrow podemos de hecho particionar nuestros datos por uno o varios criterios y exportarlos como ficheros a un directorio. Le invito a profundizar en esta tecnología.

## Consideraciones adicionales
Probablemente se pregunte por qué debería de trabajar con estos formatos si maneja pequeños volúmenes datos y un `.csv` no le causa mucho problema. Le respondo, el nivel de madurez analítica de su organización lo guiará hacia nuevos retos, probablemente un Datalake, y a ese punto es sumamente importante llegar con las bases bien definidas.


## Referencias
* [Artículos - Reisel González Pérez](https://www.linkedin.com/in/reiselgp/detail/recent-activity/posts/)