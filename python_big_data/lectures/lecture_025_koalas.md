# Koalas

## Introducción

El proyecto [Koalas](https://koalas.readthedocs.io/en/latest/index.html#) hace que los científicos de datos sean más productivos cuando interactúan con big data, al implementar la API pandas DataFrame sobre Apache Spark. pandas es la implementación de DataFrame estándar de facto (un solo nodo) en Python, mientras que Spark es el estándar de facto para el procesamiento de big data. Con este paquete, puede:

* Sea productivo de inmediato con Spark, sin curva de aprendizaje, si ya está familiarizado con los pandas.

* Tener una única base de código


### Instalación

Koalas requiere PySpark, así que asegúrese de que su PySpark esté disponible. Se deja la documentación oficial para la [instalación de Koalas](https://koalas.readthedocs.io/en/latest/getting_started/install.html)

## Primer Pasos

Esta es una breve introducción a Koalas, dirigida principalmente a nuevos usuarios. Este notebook le muestra algunas diferencias clave entre pandas y koalas. 


```python
import warnings
warnings.filterwarnings("ignore")

import pandas as pd
import numpy as np
import databricks.koalas as ks
from pyspark.sql import SparkSession
```

    WARNING:root:Found numpy version "1.20.1" installed with pyspark version "3.0.1". Some functions will not work well with this combination of numpy version "1.20.1" and pyspark version "3.0.1". Please try to upgrade pyspark version to 3.1 or above, or downgrade numpy version to below 1.20.
    WARNING:root:'PYARROW_IGNORE_TIMEZONE' environment variable was not set. It is required to set this environment variable to '1' in both driver and executor sides if you use pyarrow>=2.0.0. Koalas will set it for you but it does not work if there is a Spark context already launched.


### Creación de objetos
Creando una serie Koalas pasando una lista de valores, permitiendo que Koalas cree un índice entero predeterminado:


```python
s = ks.Series([1, 3, 5, np.nan, 6, 8])
s
```




    0    1.0
    1    3.0
    2    5.0
    3    NaN
    4    6.0
    5    8.0
    dtype: float64



Creando un Koalas DataFrame pasando un dict de objetos que se pueden convertir a series.


```python
kdf = ks.DataFrame(
    {'a': [1, 2, 3, 4, 5, 6],
     'b': [100, 200, 300, 400, 500, 600],
     'c': ["one", "two", "three", "four", "five", "six"]},
    index=[10, 20, 30, 40, 50, 60])
kdf
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>a</th>
      <th>b</th>
      <th>c</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>10</th>
      <td>1</td>
      <td>100</td>
      <td>one</td>
    </tr>
    <tr>
      <th>20</th>
      <td>2</td>
      <td>200</td>
      <td>two</td>
    </tr>
    <tr>
      <th>30</th>
      <td>3</td>
      <td>300</td>
      <td>three</td>
    </tr>
    <tr>
      <th>40</th>
      <td>4</td>
      <td>400</td>
      <td>four</td>
    </tr>
    <tr>
      <th>50</th>
      <td>5</td>
      <td>500</td>
      <td>five</td>
    </tr>
    <tr>
      <th>60</th>
      <td>6</td>
      <td>600</td>
      <td>six</td>
    </tr>
  </tbody>
</table>
</div>



Creando un DataFrame de pandas pasando una matriz numpy, con un índice de fecha y hora y columnas etiquetadas:


```python
dates = pd.date_range('20130101', periods=6)
dates
```




    DatetimeIndex(['2013-01-01', '2013-01-02', '2013-01-03', '2013-01-04',
                   '2013-01-05', '2013-01-06'],
                  dtype='datetime64[ns]', freq='D')




```python
pdf = pd.DataFrame(np.random.randn(6, 4), index=dates, columns=list('ABCD'))
pdf
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>A</th>
      <th>B</th>
      <th>C</th>
      <th>D</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>2013-01-01</th>
      <td>-1.486755</td>
      <td>-0.341802</td>
      <td>1.215518</td>
      <td>1.370429</td>
    </tr>
    <tr>
      <th>2013-01-02</th>
      <td>0.649438</td>
      <td>2.798516</td>
      <td>-0.352809</td>
      <td>1.322500</td>
    </tr>
    <tr>
      <th>2013-01-03</th>
      <td>1.433401</td>
      <td>-0.797256</td>
      <td>0.888085</td>
      <td>0.388369</td>
    </tr>
    <tr>
      <th>2013-01-04</th>
      <td>-1.323005</td>
      <td>-0.995223</td>
      <td>1.764535</td>
      <td>0.341703</td>
    </tr>
    <tr>
      <th>2013-01-05</th>
      <td>-0.782096</td>
      <td>2.209043</td>
      <td>0.749793</td>
      <td>0.074701</td>
    </tr>
    <tr>
      <th>2013-01-06</th>
      <td>-1.772560</td>
      <td>0.483620</td>
      <td>-2.035522</td>
      <td>-1.874882</td>
    </tr>
  </tbody>
</table>
</div>



Ahora, este DataFrame de pandas se puede convertir en un DataFrame de Koalas


```python
kdf = ks.from_pandas(pdf)
type(kdf)
```




    databricks.koalas.frame.DataFrame



Sin embargo, se ve y se comporta igual que un DataFrame de pandas


```python
kdf
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>A</th>
      <th>B</th>
      <th>C</th>
      <th>D</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>2013-01-01</th>
      <td>-1.486755</td>
      <td>-0.341802</td>
      <td>1.215518</td>
      <td>1.370429</td>
    </tr>
    <tr>
      <th>2013-01-02</th>
      <td>0.649438</td>
      <td>2.798516</td>
      <td>-0.352809</td>
      <td>1.322500</td>
    </tr>
    <tr>
      <th>2013-01-03</th>
      <td>1.433401</td>
      <td>-0.797256</td>
      <td>0.888085</td>
      <td>0.388369</td>
    </tr>
    <tr>
      <th>2013-01-04</th>
      <td>-1.323005</td>
      <td>-0.995223</td>
      <td>1.764535</td>
      <td>0.341703</td>
    </tr>
    <tr>
      <th>2013-01-05</th>
      <td>-0.782096</td>
      <td>2.209043</td>
      <td>0.749793</td>
      <td>0.074701</td>
    </tr>
    <tr>
      <th>2013-01-06</th>
      <td>-1.772560</td>
      <td>0.483620</td>
      <td>-2.035522</td>
      <td>-1.874882</td>
    </tr>
  </tbody>
</table>
</div>



Además, es posible crear un Koalas DataFrame desde Spark DataFrame.

Creando un Spark DataFrame a partir de pandas DataFrame


```python
spark = SparkSession.builder.getOrCreate()
sdf = spark.createDataFrame(pdf)
sdf.show()
```

    +-------------------+-------------------+-------------------+-------------------+
    |                  A|                  B|                  C|                  D|
    +-------------------+-------------------+-------------------+-------------------+
    |-1.4867554449517535|-0.3418021968776575|  1.215518224466997|  1.370429448339406|
    | 0.6494380814333224| 2.7985158670049715|-0.3528094694371542| 1.3224995302907536|
    | 1.4334013519204891|-0.7972560432558051| 0.8880845562731875|0.38836875969672957|
    |-1.3230047603358068|-0.9952230224249898| 1.7645352542471806| 0.3417029286568158|
    |-0.7820955499644796|  2.209043319946947| 0.7497925731394756|0.07470085010114477|
    |-1.7725599769490314| 0.4836204949053259|-2.0355224711493274|-1.8748818203150146|
    +-------------------+-------------------+-------------------+-------------------+
    


Creando Koalas DataFrame desde Spark DataFrame. `to_koalas()` se adjunta automáticamente a Spark DataFrame y está disponible como una API cuando se importa Koalas.


```python
kdf = sdf.to_koalas()
kdf
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>A</th>
      <th>B</th>
      <th>C</th>
      <th>D</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>-1.486755</td>
      <td>-0.341802</td>
      <td>1.215518</td>
      <td>1.370429</td>
    </tr>
    <tr>
      <th>1</th>
      <td>0.649438</td>
      <td>2.798516</td>
      <td>-0.352809</td>
      <td>1.322500</td>
    </tr>
    <tr>
      <th>2</th>
      <td>1.433401</td>
      <td>-0.797256</td>
      <td>0.888085</td>
      <td>0.388369</td>
    </tr>
    <tr>
      <th>3</th>
      <td>-1.323005</td>
      <td>-0.995223</td>
      <td>1.764535</td>
      <td>0.341703</td>
    </tr>
    <tr>
      <th>4</th>
      <td>-0.782096</td>
      <td>2.209043</td>
      <td>0.749793</td>
      <td>0.074701</td>
    </tr>
    <tr>
      <th>5</th>
      <td>-1.772560</td>
      <td>0.483620</td>
      <td>-2.035522</td>
      <td>-1.874882</td>
    </tr>
  </tbody>
</table>
</div>



Tener [dtypes](https://pandas.pydata.org/pandas-docs/stable/user_guide/basics.html) específicos. Actualmente se admiten los tipos que son comunes a Spark y pandas.


```python
kdf.dtypes
```




    A    float64
    B    float64
    C    float64
    D    float64
    dtype: object



### Ver datos

Vea las filas superiores del marco. Sin embargo, es posible que los resultados no sean los mismos que los de los pandas: a diferencia de los pandas, los datos en un marco de datos de Spark no están ordenados, no tienen una noción intrínseca de índice. Cuando se le solicite el encabezado de un marco de datos, Spark solo tomará el número solicitado de filas de una partición. No confíe en él para devolver filas específicas, use `.loc` o `iloc` en su lugar.


```python
kdf.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>A</th>
      <th>B</th>
      <th>C</th>
      <th>D</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>-1.486755</td>
      <td>-0.341802</td>
      <td>1.215518</td>
      <td>1.370429</td>
    </tr>
    <tr>
      <th>1</th>
      <td>0.649438</td>
      <td>2.798516</td>
      <td>-0.352809</td>
      <td>1.322500</td>
    </tr>
    <tr>
      <th>2</th>
      <td>1.433401</td>
      <td>-0.797256</td>
      <td>0.888085</td>
      <td>0.388369</td>
    </tr>
    <tr>
      <th>3</th>
      <td>-1.323005</td>
      <td>-0.995223</td>
      <td>1.764535</td>
      <td>0.341703</td>
    </tr>
    <tr>
      <th>4</th>
      <td>-0.782096</td>
      <td>2.209043</td>
      <td>0.749793</td>
      <td>0.074701</td>
    </tr>
  </tbody>
</table>
</div>



Muestre el índice, las columnas y los datos numéricos subyacentes.

También puede recuperar el `index`; la columna de `index` se puede atribuir a un DataFrame, ver más adelante


```python
kdf.index
```




    Int64Index([0, 1, 2, 3, 4, 5], dtype='int64')




```python
kdf.columns
```




    Index(['A', 'B', 'C', 'D'], dtype='object')




```python
kdf.to_numpy()
```




    array([[-1.48675544, -0.3418022 ,  1.21551822,  1.37042945],
           [ 0.64943808,  2.79851587, -0.35280947,  1.32249953],
           [ 1.43340135, -0.79725604,  0.88808456,  0.38836876],
           [-1.32300476, -0.99522302,  1.76453525,  0.34170293],
           [-0.78209555,  2.20904332,  0.74979257,  0.07470085],
           [-1.77255998,  0.48362049, -2.03552247, -1.87488182]])



`describe` muestra un resumen estadístico rápido de sus datos


```python
kdf.describe()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>A</th>
      <th>B</th>
      <th>C</th>
      <th>D</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>count</th>
      <td>6.000000</td>
      <td>6.000000</td>
      <td>6.000000</td>
      <td>6.000000</td>
    </tr>
    <tr>
      <th>mean</th>
      <td>-0.546929</td>
      <td>0.559483</td>
      <td>0.371600</td>
      <td>0.270470</td>
    </tr>
    <tr>
      <th>std</th>
      <td>1.295843</td>
      <td>1.600640</td>
      <td>1.369763</td>
      <td>1.181229</td>
    </tr>
    <tr>
      <th>min</th>
      <td>-1.772560</td>
      <td>-0.995223</td>
      <td>-2.035522</td>
      <td>-1.874882</td>
    </tr>
    <tr>
      <th>25%</th>
      <td>-1.486755</td>
      <td>-0.797256</td>
      <td>-0.352809</td>
      <td>0.074701</td>
    </tr>
    <tr>
      <th>50%</th>
      <td>-1.323005</td>
      <td>-0.341802</td>
      <td>0.749793</td>
      <td>0.341703</td>
    </tr>
    <tr>
      <th>75%</th>
      <td>0.649438</td>
      <td>2.209043</td>
      <td>1.215518</td>
      <td>1.322500</td>
    </tr>
    <tr>
      <th>max</th>
      <td>1.433401</td>
      <td>2.798516</td>
      <td>1.764535</td>
      <td>1.370429</td>
    </tr>
  </tbody>
</table>
</div>



Transposición de sus datos


```python
kdf.T
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>0</th>
      <th>1</th>
      <th>2</th>
      <th>3</th>
      <th>4</th>
      <th>5</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>A</th>
      <td>-1.486755</td>
      <td>0.649438</td>
      <td>1.433401</td>
      <td>-1.323005</td>
      <td>-0.782096</td>
      <td>-1.772560</td>
    </tr>
    <tr>
      <th>B</th>
      <td>-0.341802</td>
      <td>2.798516</td>
      <td>-0.797256</td>
      <td>-0.995223</td>
      <td>2.209043</td>
      <td>0.483620</td>
    </tr>
    <tr>
      <th>C</th>
      <td>1.215518</td>
      <td>-0.352809</td>
      <td>0.888085</td>
      <td>1.764535</td>
      <td>0.749793</td>
      <td>-2.035522</td>
    </tr>
    <tr>
      <th>D</th>
      <td>1.370429</td>
      <td>1.322500</td>
      <td>0.388369</td>
      <td>0.341703</td>
      <td>0.074701</td>
      <td>-1.874882</td>
    </tr>
  </tbody>
</table>
</div>



Ordenando por su índice


```python
kdf.sort_index(ascending=False)
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>A</th>
      <th>B</th>
      <th>C</th>
      <th>D</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>5</th>
      <td>-1.772560</td>
      <td>0.483620</td>
      <td>-2.035522</td>
      <td>-1.874882</td>
    </tr>
    <tr>
      <th>4</th>
      <td>-0.782096</td>
      <td>2.209043</td>
      <td>0.749793</td>
      <td>0.074701</td>
    </tr>
    <tr>
      <th>3</th>
      <td>-1.323005</td>
      <td>-0.995223</td>
      <td>1.764535</td>
      <td>0.341703</td>
    </tr>
    <tr>
      <th>2</th>
      <td>1.433401</td>
      <td>-0.797256</td>
      <td>0.888085</td>
      <td>0.388369</td>
    </tr>
    <tr>
      <th>1</th>
      <td>0.649438</td>
      <td>2.798516</td>
      <td>-0.352809</td>
      <td>1.322500</td>
    </tr>
    <tr>
      <th>0</th>
      <td>-1.486755</td>
      <td>-0.341802</td>
      <td>1.215518</td>
      <td>1.370429</td>
    </tr>
  </tbody>
</table>
</div>



Ordenar por valor


```python
kdf.sort_values(by='B')
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>A</th>
      <th>B</th>
      <th>C</th>
      <th>D</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>3</th>
      <td>-1.323005</td>
      <td>-0.995223</td>
      <td>1.764535</td>
      <td>0.341703</td>
    </tr>
    <tr>
      <th>2</th>
      <td>1.433401</td>
      <td>-0.797256</td>
      <td>0.888085</td>
      <td>0.388369</td>
    </tr>
    <tr>
      <th>0</th>
      <td>-1.486755</td>
      <td>-0.341802</td>
      <td>1.215518</td>
      <td>1.370429</td>
    </tr>
    <tr>
      <th>5</th>
      <td>-1.772560</td>
      <td>0.483620</td>
      <td>-2.035522</td>
      <td>-1.874882</td>
    </tr>
    <tr>
      <th>4</th>
      <td>-0.782096</td>
      <td>2.209043</td>
      <td>0.749793</td>
      <td>0.074701</td>
    </tr>
    <tr>
      <th>1</th>
      <td>0.649438</td>
      <td>2.798516</td>
      <td>-0.352809</td>
      <td>1.322500</td>
    </tr>
  </tbody>
</table>
</div>



### Missing Data
Koalas utiliza principalmente el valor `np.nan` para representar los datos faltantes. Por defecto, no se incluye en los cálculos.


```python
pdf1 = pdf.reindex(index=dates[0:4], columns=list(pdf.columns) + ['E'])
pdf1.loc[dates[0]:dates[1], 'E'] = 1
kdf1 = ks.from_pandas(pdf1)

kdf1
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>A</th>
      <th>B</th>
      <th>C</th>
      <th>D</th>
      <th>E</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>2013-01-01</th>
      <td>-1.486755</td>
      <td>-0.341802</td>
      <td>1.215518</td>
      <td>1.370429</td>
      <td>1.0</td>
    </tr>
    <tr>
      <th>2013-01-02</th>
      <td>0.649438</td>
      <td>2.798516</td>
      <td>-0.352809</td>
      <td>1.322500</td>
      <td>1.0</td>
    </tr>
    <tr>
      <th>2013-01-03</th>
      <td>1.433401</td>
      <td>-0.797256</td>
      <td>0.888085</td>
      <td>0.388369</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>2013-01-04</th>
      <td>-1.323005</td>
      <td>-0.995223</td>
      <td>1.764535</td>
      <td>0.341703</td>
      <td>NaN</td>
    </tr>
  </tbody>
</table>
</div>



Para eliminar las filas que tienen datos faltantes.


```python
kdf1.dropna(how='any')
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>A</th>
      <th>B</th>
      <th>C</th>
      <th>D</th>
      <th>E</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>2013-01-01</th>
      <td>-1.486755</td>
      <td>-0.341802</td>
      <td>1.215518</td>
      <td>1.370429</td>
      <td>1.0</td>
    </tr>
    <tr>
      <th>2013-01-02</th>
      <td>0.649438</td>
      <td>2.798516</td>
      <td>-0.352809</td>
      <td>1.322500</td>
      <td>1.0</td>
    </tr>
  </tbody>
</table>
</div>



Llenando los datos faltantes.


```python
kdf1.fillna(value=5)
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>A</th>
      <th>B</th>
      <th>C</th>
      <th>D</th>
      <th>E</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>2013-01-01</th>
      <td>-1.486755</td>
      <td>-0.341802</td>
      <td>1.215518</td>
      <td>1.370429</td>
      <td>1.0</td>
    </tr>
    <tr>
      <th>2013-01-02</th>
      <td>0.649438</td>
      <td>2.798516</td>
      <td>-0.352809</td>
      <td>1.322500</td>
      <td>1.0</td>
    </tr>
    <tr>
      <th>2013-01-03</th>
      <td>1.433401</td>
      <td>-0.797256</td>
      <td>0.888085</td>
      <td>0.388369</td>
      <td>5.0</td>
    </tr>
    <tr>
      <th>2013-01-04</th>
      <td>-1.323005</td>
      <td>-0.995223</td>
      <td>1.764535</td>
      <td>0.341703</td>
      <td>5.0</td>
    </tr>
  </tbody>
</table>
</div>



### Operaciones

#### Stats

Las operaciones en general excluyen los datos faltantes.

Realización de una estadística descriptiva:


```python
kdf.mean()
```




    A   -0.546929
    B    0.559483
    C    0.371600
    D    0.270470
    dtype: float64



#### Configuraciones de Spark

Varias configuraciones en PySpark se pueden aplicar internamente en Koalas. Por ejemplo, puede habilitar la optimización de Arrow para acelerar enormemente la conversión de pandas internos. 


```python
prev = spark.conf.get("spark.sql.execution.arrow.enabled")  # Keep its default value.
ks.set_option("compute.default_index_type", "distributed")  # Use default index prevent overhead.
```


```python
spark.conf.set("spark.sql.execution.arrow.enabled", True)
%timeit ks.range(300000).to_pandas()
```

    202 ms ± 27.1 ms per loop (mean ± std. dev. of 7 runs, 1 loop each)



```python
spark.conf.set("spark.sql.execution.arrow.enabled", False)
%timeit ks.range(300000).to_pandas()
```

    1.76 s ± 47.5 ms per loop (mean ± std. dev. of 7 runs, 1 loop each)



```python
ks.reset_option("compute.default_index_type")
spark.conf.set("spark.sql.execution.arrow.enabled", prev)  # Set its default value back.
```

### Agrupar datos
Por "agrupar por" nos referimos a un proceso que involucra uno o más de los siguientes pasos:

* Dividir los datos en grupos según algunos criterios

* Aplicar una función a cada grupo de forma independiente

* Combinar los resultados en una estructura de datos


```python
kdf = ks.DataFrame({'A': ['foo', 'bar', 'foo', 'bar',
                          'foo', 'bar', 'foo', 'foo'],
                    'B': ['one', 'one', 'two', 'three',
                          'two', 'two', 'one', 'three'],
                    'C': np.random.randn(8),
                    'D': np.random.randn(8)})
kdf
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>A</th>
      <th>B</th>
      <th>C</th>
      <th>D</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>foo</td>
      <td>one</td>
      <td>-0.720229</td>
      <td>-0.313725</td>
    </tr>
    <tr>
      <th>1</th>
      <td>bar</td>
      <td>one</td>
      <td>0.955598</td>
      <td>-0.771347</td>
    </tr>
    <tr>
      <th>2</th>
      <td>foo</td>
      <td>two</td>
      <td>1.330376</td>
      <td>0.313267</td>
    </tr>
    <tr>
      <th>3</th>
      <td>bar</td>
      <td>three</td>
      <td>0.422517</td>
      <td>-1.774630</td>
    </tr>
    <tr>
      <th>4</th>
      <td>foo</td>
      <td>two</td>
      <td>-1.605598</td>
      <td>-0.616041</td>
    </tr>
    <tr>
      <th>5</th>
      <td>bar</td>
      <td>two</td>
      <td>0.592688</td>
      <td>-0.170735</td>
    </tr>
    <tr>
      <th>6</th>
      <td>foo</td>
      <td>one</td>
      <td>0.407587</td>
      <td>2.380751</td>
    </tr>
    <tr>
      <th>7</th>
      <td>foo</td>
      <td>three</td>
      <td>0.540326</td>
      <td>-0.352941</td>
    </tr>
  </tbody>
</table>
</div>



Agrupar y luego aplicar la función `sum()` a los grupos resultantes.


```python
kdf.groupby('A').sum()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>C</th>
      <th>D</th>
    </tr>
    <tr>
      <th>A</th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>bar</th>
      <td>1.970804</td>
      <td>-2.716713</td>
    </tr>
    <tr>
      <th>foo</th>
      <td>-0.047537</td>
      <td>1.411311</td>
    </tr>
  </tbody>
</table>
</div>



Agrupar por múltiples columnas forma un índice jerárquico, y nuevamente podemos aplicar la función de suma.


```python
kdf.groupby(['A', 'B']).sum()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th></th>
      <th>C</th>
      <th>D</th>
    </tr>
    <tr>
      <th>A</th>
      <th>B</th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th rowspan="2" valign="top">foo</th>
      <th>one</th>
      <td>-0.312642</td>
      <td>2.067026</td>
    </tr>
    <tr>
      <th>two</th>
      <td>-0.275221</td>
      <td>-0.302774</td>
    </tr>
    <tr>
      <th>bar</th>
      <th>three</th>
      <td>0.422517</td>
      <td>-1.774630</td>
    </tr>
    <tr>
      <th>foo</th>
      <th>three</th>
      <td>0.540326</td>
      <td>-0.352941</td>
    </tr>
    <tr>
      <th rowspan="2" valign="top">bar</th>
      <th>two</th>
      <td>0.592688</td>
      <td>-0.170735</td>
    </tr>
    <tr>
      <th>one</th>
      <td>0.955598</td>
      <td>-0.771347</td>
    </tr>
  </tbody>
</table>
</div>



### Plotting
Veamos algunos gráficos sencillos con Koalas.

```python
pser = pd.Series(np.random.randn(1000),
                 index=pd.date_range('1/1/2000', periods=1000))
kser = ks.Series(pser)
kser = kser.cummax()
kser.plot()
```

<img src="images/koalas_01.png" alt="" align="center"/>

En un DataFrame, el método `plot()` es conveniente para trazar todas las columnas con etiquetas:
```python
pdf = pd.DataFrame(np.random.randn(1000, 4), index=pser.index,
                   columns=['A', 'B', 'C', 'D'])
kdf = ks.from_pandas(pdf)
kdf = kdf.cummax()
kdf.plot()
```

<img src="images/koalas_02.png" alt="" align="center"/>


### Data in/out

CSV es sencillo y fácil de usar. Parquet y ORC son formatos de archivo compactos y eficientes para leer y escribir más rápido.

#### CSV


```python
kdf.to_csv('foo.csv')
ks.read_csv('foo.csv').head(10)
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>A</th>
      <th>B</th>
      <th>C</th>
      <th>D</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>1.248795</td>
      <td>-0.402530</td>
      <td>0.473151</td>
      <td>0.111956</td>
    </tr>
    <tr>
      <th>1</th>
      <td>1.248795</td>
      <td>-0.402530</td>
      <td>0.473151</td>
      <td>0.111956</td>
    </tr>
    <tr>
      <th>2</th>
      <td>1.248795</td>
      <td>1.339012</td>
      <td>0.473151</td>
      <td>0.111956</td>
    </tr>
    <tr>
      <th>3</th>
      <td>1.248795</td>
      <td>1.339012</td>
      <td>2.435680</td>
      <td>2.141751</td>
    </tr>
    <tr>
      <th>4</th>
      <td>1.602611</td>
      <td>1.339012</td>
      <td>2.435680</td>
      <td>2.141751</td>
    </tr>
    <tr>
      <th>5</th>
      <td>1.602611</td>
      <td>1.339012</td>
      <td>2.435680</td>
      <td>2.141751</td>
    </tr>
    <tr>
      <th>6</th>
      <td>1.602611</td>
      <td>1.339012</td>
      <td>2.435680</td>
      <td>2.141751</td>
    </tr>
    <tr>
      <th>7</th>
      <td>1.602611</td>
      <td>1.339012</td>
      <td>2.435680</td>
      <td>2.141751</td>
    </tr>
    <tr>
      <th>8</th>
      <td>1.602611</td>
      <td>1.339012</td>
      <td>2.435680</td>
      <td>2.141751</td>
    </tr>
    <tr>
      <th>9</th>
      <td>1.602611</td>
      <td>1.339012</td>
      <td>2.435680</td>
      <td>2.141751</td>
    </tr>
  </tbody>
</table>
</div>



#### Parquet


```python
kdf.to_parquet('bar.parquet')
ks.read_parquet('bar.parquet').head(10)
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>A</th>
      <th>B</th>
      <th>C</th>
      <th>D</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>1.248795</td>
      <td>-0.402530</td>
      <td>0.473151</td>
      <td>0.111956</td>
    </tr>
    <tr>
      <th>1</th>
      <td>1.248795</td>
      <td>-0.402530</td>
      <td>0.473151</td>
      <td>0.111956</td>
    </tr>
    <tr>
      <th>2</th>
      <td>1.248795</td>
      <td>1.339012</td>
      <td>0.473151</td>
      <td>0.111956</td>
    </tr>
    <tr>
      <th>3</th>
      <td>1.248795</td>
      <td>1.339012</td>
      <td>2.435680</td>
      <td>2.141751</td>
    </tr>
    <tr>
      <th>4</th>
      <td>1.602611</td>
      <td>1.339012</td>
      <td>2.435680</td>
      <td>2.141751</td>
    </tr>
    <tr>
      <th>5</th>
      <td>1.602611</td>
      <td>1.339012</td>
      <td>2.435680</td>
      <td>2.141751</td>
    </tr>
    <tr>
      <th>6</th>
      <td>1.602611</td>
      <td>1.339012</td>
      <td>2.435680</td>
      <td>2.141751</td>
    </tr>
    <tr>
      <th>7</th>
      <td>1.602611</td>
      <td>1.339012</td>
      <td>2.435680</td>
      <td>2.141751</td>
    </tr>
    <tr>
      <th>8</th>
      <td>1.602611</td>
      <td>1.339012</td>
      <td>2.435680</td>
      <td>2.141751</td>
    </tr>
    <tr>
      <th>9</th>
      <td>1.602611</td>
      <td>1.339012</td>
      <td>2.435680</td>
      <td>2.141751</td>
    </tr>
  </tbody>
</table>
</div>



#### ORC


```python
kdf.to_spark_io('zoo.orc', format="orc")
ks.read_spark_io('zoo.orc', format="orc").head(10)
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>A</th>
      <th>B</th>
      <th>C</th>
      <th>D</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>1.248795</td>
      <td>-0.402530</td>
      <td>0.473151</td>
      <td>0.111956</td>
    </tr>
    <tr>
      <th>1</th>
      <td>1.248795</td>
      <td>-0.402530</td>
      <td>0.473151</td>
      <td>0.111956</td>
    </tr>
    <tr>
      <th>2</th>
      <td>1.248795</td>
      <td>1.339012</td>
      <td>0.473151</td>
      <td>0.111956</td>
    </tr>
    <tr>
      <th>3</th>
      <td>1.248795</td>
      <td>1.339012</td>
      <td>2.435680</td>
      <td>2.141751</td>
    </tr>
    <tr>
      <th>4</th>
      <td>1.602611</td>
      <td>1.339012</td>
      <td>2.435680</td>
      <td>2.141751</td>
    </tr>
    <tr>
      <th>5</th>
      <td>1.602611</td>
      <td>1.339012</td>
      <td>2.435680</td>
      <td>2.141751</td>
    </tr>
    <tr>
      <th>6</th>
      <td>1.602611</td>
      <td>1.339012</td>
      <td>2.435680</td>
      <td>2.141751</td>
    </tr>
    <tr>
      <th>7</th>
      <td>1.602611</td>
      <td>1.339012</td>
      <td>2.435680</td>
      <td>2.141751</td>
    </tr>
    <tr>
      <th>8</th>
      <td>1.602611</td>
      <td>1.339012</td>
      <td>2.435680</td>
      <td>2.141751</td>
    </tr>
    <tr>
      <th>9</th>
      <td>1.602611</td>
      <td>1.339012</td>
      <td>2.435680</td>
      <td>2.141751</td>
    </tr>
  </tbody>
</table>
</div>


